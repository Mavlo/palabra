/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.examenproyecto.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pablo
 */
@Entity
@Table(name = "inscripcionpalabra")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Inscripcionpalabra.findAll", query = "SELECT i FROM Inscripcionpalabra i"),
    @NamedQuery(name = "Inscripcionpalabra.findByRut", query = "SELECT i FROM Inscripcionpalabra i WHERE i.rut = :rut"),
    @NamedQuery(name = "Inscripcionpalabra.findByPalabra", query = "SELECT i FROM Inscripcionpalabra i WHERE i.palabra = :palabra"),
    @NamedQuery(name = "Inscripcionpalabra.findByResultado", query = "SELECT i FROM Inscripcionpalabra i WHERE i.resultado = :resultado")})
public class Inscripcionpalabra implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "rut")
    private String rut;
    @Size(max = 2147483647)
    @Column(name = "palabra")
    private String palabra;
    @Size(max = 2147483647)
    @Column(name = "resultado")
    private String resultado;

    public Inscripcionpalabra() {
    }

    public Inscripcionpalabra(String rut) {
        this.rut = rut;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rut != null ? rut.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inscripcionpalabra)) {
            return false;
        }
        Inscripcionpalabra other = (Inscripcionpalabra) object;
        if ((this.rut == null && other.rut != null) || (this.rut != null && !this.rut.equals(other.rut))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.examenproyecto.entity.Inscripcionpalabra[ rut=" + rut + " ]";
    }
    
}
