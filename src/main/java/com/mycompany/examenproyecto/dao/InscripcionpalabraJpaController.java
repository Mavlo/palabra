/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.examenproyecto.dao;

import com.mycompany.examenproyecto.dao.exceptions.NonexistentEntityException;
import com.mycompany.examenproyecto.dao.exceptions.PreexistingEntityException;
import com.mycompany.examenproyecto.entity.Inscripcionpalabra;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Pablo
 */
public class InscripcionpalabraJpaController implements Serializable {

    public InscripcionpalabraJpaController() {
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Inscripcionpalabra inscripcionpalabra) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(inscripcionpalabra);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findInscripcionpalabra(inscripcionpalabra.getRut()) != null) {
                throw new PreexistingEntityException("Inscripcionpalabra " + inscripcionpalabra + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Inscripcionpalabra inscripcionpalabra) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            inscripcionpalabra = em.merge(inscripcionpalabra);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = inscripcionpalabra.getRut();
                if (findInscripcionpalabra(id) == null) {
                    throw new NonexistentEntityException("The inscripcionpalabra with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Inscripcionpalabra inscripcionpalabra;
            try {
                inscripcionpalabra = em.getReference(Inscripcionpalabra.class, id);
                inscripcionpalabra.getRut();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The inscripcionpalabra with id " + id + " no longer exists.", enfe);
            }
            em.remove(inscripcionpalabra);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Inscripcionpalabra> findInscripcionpalabraEntities() {
        return findInscripcionpalabraEntities(true, -1, -1);
    }

    public List<Inscripcionpalabra> findInscripcionpalabraEntities(int maxResults, int firstResult) {
        return findInscripcionpalabraEntities(false, maxResults, firstResult);
    }

    private List<Inscripcionpalabra> findInscripcionpalabraEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Inscripcionpalabra.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Inscripcionpalabra findInscripcionpalabra(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Inscripcionpalabra.class, id);
        } finally {
            em.close();
        }
    }

    public int getInscripcionpalabraCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Inscripcionpalabra> rt = cq.from(Inscripcionpalabra.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
